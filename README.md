# Dual Color Headlight Controller

Dual Color Headlight Controller is an add-on to dual color Chinese LED bulbs. It uses the fog light signal to turn on the other color(s) instead of the default turn off and then on again, which leads to unpredictable results.

#### Main features:
- Uses fog light signal to switch colors
- Can be combined with other modules that use fog lights as DRL
- Converts dual color LED to triple color LED
- Service procedure allows the driver to easily change color sequence without re-flashing

## Supported cars

If you use the supplied helper, you can run it on any car. If not, a car with GND as a controlling signal to the front fog relay is required. This is done due to some DRL modules bypassing the fog lights relay and supplying 12V to the bulbs directly, which is misleading. Monitoring the fog lights relay is the way to go. Only when it is active, then the driver wants a different color.

I've developed the module for my 2001 Mazda 626 GF, but I've also installed it on my 2013 Mazda 2 DE.

## Installation video

It explains way more that what I can write here.

[![](http://img.youtube.com/vi/CyPpq67lDeg/0.jpg)](https://www.youtube.com/watch?v=CyPpq67lDeg "")

https://www.youtube.com/watch?v=CyPpq67lDeg

## Software

Source code is written with [Arduino IDE](https://www.arduino.cc/) 2.0.

Code is very simple, and it fits just fine in a single file - [DualColorHeadlightController.ino](src/DualColorHeadlightController/DualColorHeadlightController.ino)

## Schematics

![](schematics/dual-color-headlight-controller.png)

Connection for the two outputs depends on the PCB inside the stock module. Check the video to see how it is done in my case.

### Helper

The helper circuit may not be required if your controlling GND signal is always clean. As seen in the video, mine is dirty. The same goes for the Mazda 2 DE. **You can also simply use relay for a helper**. The output of the schematics goes to the input of the Arduino(s).

![](schematics/dual-color-headlight-controller-helper.png)

Schematics have been done with [Circuit Diagram](https://www.circuit-diagram.org/). Check [schematics](schematics) folder.

## Installation

The Arduino Pro Mini fits within the box of the stock module, along with it. Check the video for more information.

### Flashing without the bootloader (required for fast startup)

Bootloader takes 1.5 to 2 seconds to boot, during which no light is emitted from the LEDs. Not good. You need to flash the Arduino without the bootloader for an instant boot. The procedure:

1. Get a second Arduino. I repurposed another Pro Mini with a burnt out pin.
2. Flash that second Arduino with the **ArduinoISP** example (from the **File** menu). This Arduino will now become the programmer.
3. Solder around 10nF capacitor between **GND** and **RST** pins of the **programmer** Arduino. I didn't have 10nF, 4.7nF did the trick.
4. Connect Programmer pins -> Arduino Pro Mini pins as follows:
   1. GND -> GND
   2. Vcc -> Vcc
   3. Pin 10 -> RST
   4. Pin 11 -> Pin 11
   5. Pin 12 -> Pin 12
   6. Pin 13 -> Pin 13
5. In **Arduino IDE**, select Programmer to be **Arduino as ISP**
6. Upload by using **Upload Using Programmer** option in **Sketch** menu.

## Switch color sequence

The flashed array of colors can be changed within the car, so take a look at what you are flashing prior to flashing.

To change the sequence, simply turn on the headlights and in the first 10s turn on and off the fog lights 5 times. Headlights will go out, then show the sequence to be used. If you do not like it. Turn off the headlights and repeat the procedure. See the video for more details.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.