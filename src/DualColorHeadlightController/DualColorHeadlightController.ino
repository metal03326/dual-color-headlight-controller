#include <EEPROM.h>

#define VER 1.0

#define i_fog 2
#define o_white A3
#define o_yellow 4
#define o_pullup 3

// This code is copied from http://masteringarduino.blogspot.com/2013/10/fastest-and-smallest-digitalread-and.html
#define portOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &PORTD : (((P) > 7 && (P) < 14) ? &PORTB : &PORTC))
#define ddrOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &DDRD : (((P) > 7 && (P) < 14) ? &DDRB : &DDRC))
#define pinOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &PIND : (((P) > 7 && (P) < 14) ? &PINB : &PINC))
#define pinIndex(P) ((uint8_t)(P > 13 ? P - 14 : P & 7))
#define pinMask(P) ((uint8_t)(1 << pinIndex(P)))
#define pinAsInput(P) *(ddrOfPin(P)) &= ~pinMask(P)
#define pinAsInputPullUp(P) \
  *(ddrOfPin(P)) &= ~pinMask(P); \
  digitalHigh(P)
#define pinAsOutput(P) *(ddrOfPin(P)) |= pinMask(P)
#define digitalLow(P) *(portOfPin(P)) &= ~pinMask(P)
#define digitalHigh(P) *(portOfPin(P)) |= pinMask(P)
#define isHigh(P) ((*(pinOfPin(P)) & pinMask(P)) > 0)
#define isLow(P) ((*(pinOfPin(P)) & pinMask(P)) == 0)
#define digitalState(P) ((uint8_t)isHigh(P))

// Order of switching lights. 
// 0 means disabled (color will not be used)
// 1 is the default light (when fog lights are off)
// 2 is light on first fog light turn on
// 3 is light on the second fog light turn on.
// 2 and 3 keep cycle when driver continues to toggle fog lights on and off
// First entry will be the default, if not changed from the service procedure
// Columns represent emitted lights:
// 0 is WHITE
// 1 is AMBER (both white and yellow)
// 2 is YELLOW
// Remove modes you will not use from here
int modes[][3] = {
  {1, 2, 3}, // [white] amber-yellow
  // {1, 3, 2}, // [white] yellow-amber
  // {1, 2, 0}, // [white] amber
  // {1, 0, 2}, // [white] yellow

  // {3, 2, 1}, // [yellow] amber-white
  // {2, 3, 1}, // [yellow] white-amber
  // {0, 2, 1}, // [yellow] amber
  // {2, 0, 1}, // [yellow] white

  // {3, 1, 2}, // [amber] yellow-white
  // {2, 1, 3}, // [amber] white-yellow
  // {0, 1, 2}, // [amber] yellow
  // {2, 1, 0}, // [amber] white

  // {0, 0, 1}, // [yellow]
  // {0, 1, 0}, // [amber]
  {1, 0, 0}  // [white]
};
int* mode;
// Make sure we still have light even if something goes wrong
int defaultLightsIndex = 0;
int lastLightsIndex = 0;
// Interrupt is not working properly without some filters, but we do not have the 
// space for them, so we do the filtering in the software
int lastState = 1;
unsigned long lastFogIndexChange;
int serviceProcedureCounter = 0;
// After changing the mode in the Service Procedure, we need to display it as a
// feedback to the driver.
bool displaySequence = false;

void setLights(int index) {
  switch (index) {
    // White
    case 0:
      digitalHigh(o_white);
      digitalLow(o_yellow);
    break;
    // Amber
    case 1:
      digitalHigh(o_white);
      digitalHigh(o_yellow);
    break;
    // Yellow
    case 2:
      digitalLow(o_white);
      digitalHigh(o_yellow);
    break;
    // Lights out. Used to display color sequence
    default:
      digitalLow(o_white);
      digitalLow(o_yellow);
  }
}

void setNextFogLights() {
  int currentLightsIndex = lastLightsIndex;
  unsigned long time = millis();

  // Forget last index if it was changed more that 10 seconds ago
  if (time - lastFogIndexChange > 10000) {
    currentLightsIndex = defaultLightsIndex;
  }

  lastFogIndexChange = time;

  int nextValue = mode[currentLightsIndex] + 1;
  // Handle single color only. In case we search for 2 or 3 and never find them, set 
  // lights as the default, which effectively ignores fog light switch.
  int nextIndex = defaultLightsIndex;

  // If we were 3, loop back to 2
  if (nextValue > 3) {
    nextValue = 2;
  }

  // Search the new index with this value
  for (int i = 0; i < 3; i++) {
    if (mode[i] == nextValue) {
      nextIndex = i;
      break;
    }
  }

  // Either single or dual color setup if nextIndex was not changed from the defaultLightsIndex
  // Dual color setup: If we searched for 3, but haven't found it, go back to last index,
  // as it was 2 and we had it.
  if (nextIndex == defaultLightsIndex && nextValue == 3) {
    nextIndex = currentLightsIndex;
  }

  lastLightsIndex = nextIndex;
  setLights(nextIndex);
}

void initDefaults() {
  // Search default light
  for (int i = 0; i < 3; i++) {
    if (mode[i] == 1) {
      defaultLightsIndex = i;
      lastLightsIndex = i;
      break;
    }
  }
}

void setup() {
  pinAsOutput(o_white);
  pinAsOutput(o_yellow);
  pinAsOutput(o_pullup);

  // We have external pullup resistor, but just in case make this one pullup as well
  pinAsInputPullUp(i_fog);

  // Set pullup resistor for the fog lights ground input
  digitalHigh(o_pullup);

  int index = EEPROM.read(0);

  if (index > sizeof(modes)/sizeof(modes[0])) {
    index = 0;
  }

  mode = modes[index];

  initDefaults();

  if (digitalState(i_fog)) {
    setLights(defaultLightsIndex);
  }
  else {
    setNextFogLights();
  }
}

void loop() {
  if (displaySequence) {
    // Setup varaibles
    initDefaults();

    // Turn off the lights to indicate start of display procedure
    setLights(-1);
    delay(2000);

    // Show default color
    setLights(defaultLightsIndex);
    delay(2000);

    int secondIndex = defaultLightsIndex;
    int thirdIndex = defaultLightsIndex;

    // Find order of other colors
    for (int i = 0; i < 3; i++) {
      if (mode[i] == 2) {
        secondIndex = i;
      } else if (mode[i] == 3) {
        thirdIndex = i;
      }
    }

    // Do we have a second color? Show it!
    if (secondIndex != defaultLightsIndex) {
      setLights(secondIndex);
      delay(2000);

      // Do we have a third color? Show it!
      if (thirdIndex != defaultLightsIndex) {
        setLights(thirdIndex);
        delay(2000);
      }
    }

    // Turn off the lights to indicate end of display procedure
    setLights(-1);
    delay(2000);

    // Reset flags for fog lights, as driver may have toggled the
    // switch while in the service procedure
    lastState = digitalState(i_fog);

    // Set proper lights based on the fog switch
    if (lastState) {
      setLights(defaultLightsIndex);
    }
    else {
      setNextFogLights();
    }

    // Do not enter here again
    displaySequence = false;
  } else {
    int currentState = digitalState(i_fog);

    if (currentState != lastState) {
      // LEDs have an impulse power supply, making things more noisy. Filter by
      // software, to keep hardware simple (and fit within the box).
      for (int i = 0; i < 5; i++) {
        delay(20);
        currentState = digitalState(i_fog);
        // False positive, end it now
        if (currentState == lastState) {
          break;
        }
      }

      // Check again - if still different then all checks have passed and
      // we assume signal is correct
      if (currentState != lastState) {
        lastState = currentState;

        if (currentState) {
          setLights(defaultLightsIndex);
        } else {
          bool inServiceProcedure = false;

          // Service Procedure can be accessed only in the first 10 seconds
          if (millis() < 10000 && serviceProcedureCounter < 5) {
            serviceProcedureCounter++;

            if (serviceProcedureCounter == 5) {
              inServiceProcedure = true;

              int index = EEPROM.read(0);

              // If never changed, EEPROM will hold 255, which maps to 0 in setup()
              if (index > sizeof(modes)/sizeof(modes[0])) {
                index = 0;
              }

              int nextIndex = index + 1;

              if (nextIndex >= sizeof(modes)/sizeof(modes[0])) {
                nextIndex = 0;
              }

              EEPROM.write(0, nextIndex);

              mode = modes[nextIndex];

              displaySequence = true;
            }
          }

          // Ignore lights
          if (!inServiceProcedure) {
            setNextFogLights();
          }
        } 
      }
    }
  }
}
